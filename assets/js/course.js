
// course.html?courseId=601d0bfd54f97a29109bcad9
//URL Query parameters
//  ? - start of query string
// courseId= (parameter name)
// 601d0bfd54f97a29109bcad9 = value

//window.location.search return to 
// console.log(window.location.search)

//instantiate or create a new URLSearchParams object.
//This object, URLSearchParams, is used an interfere to gain access to methods that allow us to specific parts of the query string.
let params = new URLSearchParams(window.location.search)

//The .has method for URLSearchParams checks if the courseId key exists in our URL Query string.
console.log(params.has('courseId'))

//The get method for URLSearchParams returns the value of the key passed in as an argument.
// console.log(params.get('courseId'))

//Store the courseId from the URL Query string in a variable:
let courseId = params.get('courseId')


//get the token from localStorage
let token = localStorage.getItem('token')

let courseName = document.querySelector('#courseName')
let courseDesc = document.querySelector('#courseDesc')
let coursePrice = document.querySelector('#coursePrice')
let enrollContainer = document.querySelector('#enrollContainer')
let enrollees = document.querySelector('#enrolees');
let adminUser = localStorage.getItem("isAdmin") === "true"

// not define v.s undefined
// undefined = variable was declared without initial value.
// not defined =the variable does not exist.

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//Get the details of a single course.
// fetch(`http://localhost:8000/api/courses/${courseId}`)
// .then(res => res.json())
// .then(data => {
// 	console.log(data)
// 	courseName.innerHTML = data.name
// 	courseDesc.innerHTML = data.description
// 	coursePrice.innerHTML = data.price
// 	enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`
// 	document.querySelector('#enrollButton').addEventListener("click", ()=>{
// 		//add fetch request to enroll our user
// 		fetch('http://localhost:8000/api/users/enroll', {
// 			method: 'POST',
// 			headers: {
// 				'Content-Type': 'application/json',
// 				'Authorization': `Bearer ${token}`
// 			},
// 			body: JSON.stringify({
// 				courseId: courseId
// 			})
// 		})
// 		.then(res => res.json())
// 		.then(data => {
// 			//redirect the user to the courses page after enrolling.
// 			if( data === true){
// 				alert('Thank you for enrolling to the course.')
// 				window.location.replace('./courses.html')
// 			} else {
// 				//server error while enrolling to course
// 				alert('something went wrong')
// 			}
			
// 		})
// 	})
// })

//----------------------------------




if (adminUser === true) {
	console.log("adminUser")

	fetch(`https://peaceful-scrubland-07372.herokuapp.com/api/courses/${courseId}`)
	.then(res => res.json())
	.then(data => {


		courseName.innerHTML = data.name;
		courseDesc.innerHTML = data.description;
		coursePrice.innerHTML = data.price;

		console.log(data)

		if(data.enrollees.length === 0){

			enrollees.innerHTML = `<h5 class="card-title">No Enrollees</h5>`

		} else {
			data.enrollees.forEach(enrollee => {
				console.log(enrollees)
			//>>>>>>>>>>>>>>>>>>getting user id then name and last name
				let enrolleeId = enrollee.userId
				console.log (enrolleeId)



			//>>>>>>>>>>>>>>>>>>
				fetch(`https://peaceful-scrubland-07372.herokuapp.com/api/users/details/${enrolleeId}`,{

					headers: {
						"Content-Type": 'application/json',
						'Authorization' : `Bearer ${token}`
					}
				})
				.then(res => res.json())
				.then(data => {


					let enrolleeName = `${data.firstName} ${data.lastName}`;

					if (data) {
						enrollees.innerHTML +=
							` 
								<div class="card my-5">
									<div class="card-body">
										<h5 class="card-title">${enrolleeName}</h5>
										<p class="card-text text-center text-info">Enrolled On: ${Date(enrollee.enrolledOn)}</p>
									</div>
								</div>
							`
					} else {
						alert("Something went wrong");
					}
				})
			})
		}

		enrollContainer.innerHTML = `<button id="backButton" class="btn btn-block text-white">Back to Courses</button>`;
		console.log(enrollContainer);
		//button color
		document.querySelector('#backButton').style.background = '#1D3B72';
		
		document.querySelector('#enrollContainer').addEventListener("click", () =>{
				window.location.replace('./courses.html');
		})
	});

} else {
	fetch(`https://peaceful-scrubland-07372.herokuapp.com/api/courses/${courseId}`)
	.then(res => res.json())
	.then(data => {

		courseName.innerHTML = data.name;
		courseDesc.innerHTML = data.description;
		coursePrice.innerHTML = data.price;
		enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block text-white">Enroll</button>`;
		//button color
		document.querySelector('#enrollButton').style.background = '#1D3B72';

		document.querySelector('#enrollButton').addEventListener("click", () => {

			if(token) {
				fetch('https://peaceful-scrubland-07372.herokuapp.com/api/users/enroll', {

					method: "POST",
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${token}`
					},
					body: JSON.stringify({
						courseId: courseId,

					})
				})
				.then(res => res.json())
				.then(data => {

					if(data === true){
						alert('Thankyou for enrolling to the course!');
						window.location.replace('./courses.html');
					}else {
						alert('Something went wrong.')
					}

				});
			} else{
				window.location.replace('./register.html')
			}
		});
	});
};