let formSubmit = document.querySelector("#createCourse")

//add event listener
formSubmit.addEventListener("submit",(e)=>{
	e.preventDefault()
	let courseName = document.querySelector("#courseName").value
	let description = document.querySelector('#courseDescription').value
	let price = document.querySelector("#coursePrice").value
	
	//Get the JWT from our localStorage
	let token =localStorage.getItem("token")
	console.log(token)

	//Create a fetch request to add a new course
	fetch('https://peaceful-scrubland-07372.herokuapp.com/api/courses', {
		method: 'POST',
		headers: {
			'Content-type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			name: courseName,
			description: description,
			price: price
		})
	})
	.then(res => res.json())
	.then(data => {

		//If the creation is successful, redirect the admin to the courses page.
		if(data === true){

			window.location.replace('./courses.html')

		} else {
			alert("Course Creation Failed. Something Went Wrong.")
		}
		
	
	})

})