let token = localStorage.getItem("token")
let adminUser = localStorage.getItem("isAdmin") ==="true"
let addButton = document.querySelector("#adminButton");
let cardFooter;

//This if statement will allow us to show a button for adding a course;a button to redirect us to the addCourse page if the user is admin, however, if he/she is a guest or a regular user, they should not see a button.
if(adminUser === false || adminUser === null){

	addButton.innerHTML = null

} else {
	addButton.innerHTML = 
	`
		<div class="col-md-2 offset-md-10 mt-3">
			<a href="./addCourse.html" id="addButton" class="btn btn-block text-white">Add Course</a>
		</div>
	`
	document.querySelector('#addButton').style.background = '#1D3B72';
}

fetch('https://peaceful-scrubland-07372.herokuapp.com/api/courses')
	.then(res => res.json())
	.then(data => {

		console.log(data);

		

		//A variable that will store the data to be rendered
		let courseData;
		//if the number of courses is less than 1, display no courses available.
		if(data.length < 1) {
			courseData = "No courses available"
		} else {

			courseData = data.map(course => {

				console.log(course);
				console.log(course.isActive)

				if(adminUser === false || !adminUser) {
					//*user
					console.log(course.isActive)
					if (course.isActive === true) {
						console.log(course.isActive)
						cardFooter =
							`
								<a href="./course.html?courseId=${course._id}" value={course._id} class="btn text-white btn-block editButton" style="background-color: #1D3B72!important;">
									Select Course
								</a>
							`
						return (
							`
							<div class="col-md-6 my-3">
								<div class="card h-100">
									<div class="card-body">
										<h5 class="card-title">${course.name}</h5>
										<p class="card-text text-left">
											${course.description}
										</p>
										<p class="card-text text-right">
											₱ ${course.price}
										</p>
									</div>
									<div class="card-footer" id="testbutton">
										${cardFooter}
									</div>
								</div>
							</div>
						`
						)
					}
				} else {
					//*admin				
					console.log('course.isActive', course.isActive)
					if (course.isActive === false) {
						// console.log('disable')
						cardFooter =
							`
						<a href="./editCourse.html?courseId=${course._id}" value={course._id} class="btn text-white btn-block editButton" style="background-color: #477AC2;">
							Edit
						</a>
						<a href="./enableCourse.html?courseId=${course._id}" value={course._id} class="btn text-white btn-block deleteButton" style="background-color: #2A9D8F;">
							Enable
						</a>
						<a href="./course.html?courseId=${course._id}" value={course._id} class="btn text-white btn-block editButton" style="background-color: #1D3B72;">
								View Course
							</a>
						`
					} else {
						// isActive = true
						// console.log('enable')
						cardFooter =
							`
						<a href="./editCourse.html?courseId=${course._id}" value={course._id} class="btn text-white btn-block editButton" style="background-color: #477AC2;">
							Edit
						</a>
						
						<a href="./deleteCourse.html?courseId=${course._id}" value={course._id} class="btn text-white btn-block deleteButton" style="background-color: #E76F51;">
							Disable
						</a>
						<a href="./course.html?courseId=${course._id}" value={course._id} class="btn text-white btn-block editButton" style="background-color: #1D3B72;">
								View Course
							</a>
					`

					}
					return (
						`
						<div class="col-md-6 my-3">
							<div class="card h-100">
								<div class="card-body">
									<h5 class="card-title">${course.name}</h5>
									<p class="card-text text-left">
										${course.description}
									</p>
									<p class="card-text text-right">
										₱ ${course.price}
									</p>
								</div>
								<div class="card-footer">
									${cardFooter}
								</div>
							</div>
						</div>
					`
					)
				}


					// since the collection is an array, we can use the join method to indicate the seperator for each element(remove the comma from map() method.
			}).join("")
		}

		let container = document.querySelector("#coursesContainer")

		container.innerHTML = courseData;


})

/*
	Activity:

	use the fetch Method to get all the courses and show the data into the console using console.log()

	If you are done, create a new folder (s19) in gitlab
	inside s19 folder, create a new repo: d1

	In your local machine, connect your d1 to your online repo:
	git remote add origin <url>

	Then add, commit and push it into your new repo.

	Link to boodle as:
	WD057-11 | Express.js - Booking System API Integration


*/



